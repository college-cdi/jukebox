import express from 'express';
import ejs from 'ejs';
import mongoose from 'mongoose';
import PageController from './controllers/pageController.js';

// import {router} from './router.js';
// import {homeController}
const app = new express();
const PORT = process.env.PORT ? process.env.PORT : 4000;
const MONGODB_HOSTNAME = process.env.MONGODB_HOSTNAME ? process.env.MONGODB_HOSTNAME : 'localhost';
const MONGODB_PORT = process.env.MONGODB_PORT ? process.env.MONGODB_PORT : 27017;

let pageController = new PageController();

app.set("view engine", "ejs");
app.use(express.static("public"));

mongoose.set("strictQuery", false);
mongoose.connect(
  `mongodb://${MONGODB_HOSTNAME}:${MONGODB_PORT}/${process.env.MONGODB_DATABASE}`,
).then(console.log(`DB ${process.env.MONGODB_DATABSE} connected.`));

/**
 * simple check-up to see if server is online.
 */
app.get('/ping', (req,res) => {
  res.end('pong');
})

app.get('/', pageController.viewIndex);

app.get('/contact-us', pageController.viewContactUs);

app.get('/song-list', pageController.viewSongList);

app.use((req, res) => res.end('error404')); // redirect to "404" if route is not found.

// app.use((req, res) => res.render('error404')); // redirect to "404" if route is not found.
  app.listen( PORT, () => {
    console.log(`App listening on port : ${PORT}`);
  });