import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const SongSchema = new Schema({
  "title": {
    type: String, 
    required: true,
    index: true, 
    unique: true
  },
  "img": {
    type: String,
  },
  "artist": {
    type: String, 
    required: true
  },
  "year": {
    type: String,
    required: true
  }
}) 

const Song = mongoose.model('Song', SongSchema);
export default Song;