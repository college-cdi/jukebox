import Song from '../models/songs.js';

export default class PageController {

  viewIndex(req, res) {
    res.render("index");
  }


  viewContactUs(req, res) {
     res.render("contactUs");
  }


  async viewSongList(req, res) {
    let songs = await Song.find({});
    console.log(songs);
    res.render("songList", {songs: songs});
  }

}